<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Post;
use App\Models\PostLike;
use App\Models\PostComment;

class PostController extends Controller
{
    // activity welcome page
    public function welcome()
    {
        // retrieve 3 randomized posts
        $posts = Post::inRandomOrder()->limit(3)->get();
        return view('welcome')->with('posts', $posts);
    }

    public function create()
    {
        return view('posts.create');
    }

    public function store(Request $request)
    {   
        // if theres an authenticated user
        if(Auth::user()){
            // create a new Post object from the Post model
            $post = new Post;
            // define the properties of the $post object using received form data
            $post->title = $request->input('title');
            $post->content = $request->input('content');
            // get the id of the authenticated user and set it as the value of the user_id col
            $post->user_id = (Auth::user()->id);
            // save the post object to the database
            $post->save();
            return redirect('/posts');
        }else{
            return redirect('/login');
        }
    }
    // return all posts
    public function index()
    {   
        // get all posts from the db
        //$posts = Post::all();
        $posts = Post::where('isActive', true)->get();
        return view('posts.index')->with('posts', $posts);
    }

    public function myPosts()
    {
        // if user is logged in
        if(Auth::user()){
            // retrieve the user's own posts
            $posts = Auth::user()->posts;;
            return view('posts.index')->with('posts', $posts);
        }else{
            return redirect('/login');
        }
    }

    public function show($id)
    {
        $post = Post::find($id);
        return view('posts.show')->with('post', $post);
    }

    // edit action; finding post using id passed in via the URL parameter
    public function edit($id)
    {
        $post = Post::find($id);
        return view('posts.edit')->with('post', $post);
    }

    //pass both the form data in the request, as well as the id of the post to be updated
    public function update(Request $request, $id) 
        {
            $post = Post::find($id);

            //if authenticated user's id is the same as the post's user_id
            if(Auth::user()->id == $post->user_id){
                $post->title = $request->input('title');
                $post->content = $request->input('content');
                $post->save();
            }

            return redirect('/posts');
        }

        public function destroy($id)
        {
            $post = Post::find($id);

            //if authenticated user's id is the same as the post's user_id
            if(Auth::user()->id == $post->user_id){
                $post->delete();
            }

            return redirect('/posts');        
        }

    public function archive($id)
    {
        $post = Post::find($id);
        if(Auth::user()->id == $post->user_id){
            $post->isActive = false;
            $post->save();
        }
    }

    public function like($id)
    {
        $post = Post::find($id);
        $user_id = Auth::user()->id;
        // check if the authenticatd user is NOT the post author
        if($post->user_id != $user_id){
            // check if a post has already been liked by this user
            if($post->likes->contains("user_id", $user_id)){
                PostLike::where('post_id', $post->id)->where('user_id', $user_id)->delete();
                // if the user has not likes the post yet
            } else {
                // create a new postLike object
                $postLike = new PostLike;
                // define the properties of the postlike object
                $postLike->post_id = $post->id;
                $postLike->user_id = $user_id;
                $postLike->save();
            }
            return redirect("/posts/$id");
        }
    }


    public function comment(Request $request, $id)
    {
        $post = Post::find($id);
        $user_id = Auth::user()->id;
    
        $postComment = new PostComment;
        $postComment->post_id = $post->id;
        $postComment->user_id = $user_id;
        $postComment->content = $request->input('content');
        $postComment->save();
        
        return redirect("/posts/$id");
        
    }

}
